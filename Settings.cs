﻿/*
 *  This file is a part of TXCobalt server.
 *  Copyright (C) 2015 Astie Teddy
 *  This file is under the MIT licence, you should have recieved a copy of the licence, 
 *  if you do not have recieved an copy of the licence follow this link : 
 *  https://opensource.org/licenses/MIT
 */

using System;
using System.Collections.Generic;
using System.IO;

namespace TXCobalt.Server 
{
    public static class ServerSettings
    {
        public static readonly Dictionary<string,string> default_configs = new Dictionary<string, string>
        {
            { "IP", "0.0.0.0" },
            { "Port", "5050" },
            { "MapPath", "default.map" },
            { "RulesPath", "default_rules.json" },
            { "UseLog", "true" },
            { "ShowDebug", "false" },
            { "NmsUpdateTime", "50" },
            { "NmsMaxMessages", "8" },
            { "Password", "" },
            { "MOTD", "" }
        };
            
        public static Dictionary<string,string> ReadSettings(string path)
        {
            Dictionary<string,string> settings = default_configs;

            if (File.Exists(path))
                foreach (KeyValuePair<string,string> keyvalue in ReadProperties(path))
                    if (settings.ContainsKey(keyvalue.Key))
                        settings[keyvalue.Key] = keyvalue.Value;
                    else
                        settings.Add(keyvalue.Key, keyvalue.Value);
            else
                CreateDefault(path);
            
            return settings;
        }

        public static void CreateDefault(string path)
        {
            using (FileStream stream = File.Create(path))
            {
                TextWriter writer = new StreamWriter(stream);

                writer.WriteLine("# TXCobalt server properties");
                writer.WriteLine("# Generated the {0} at {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToShortTimeString());

                writer.WriteLine();

                writer.WriteLine("# Listening IP of the server");
                writer.WriteLine("{0}={1}", "IP", default_configs["IP"]);

                writer.WriteLine();

                writer.WriteLine("# Listening port of the server");
                writer.WriteLine("{0}={1}", "Port", default_configs["Port"]);

                writer.WriteLine();

                writer.WriteLine("# Access password (used to restrict access using an password)");
                writer.WriteLine("{0}={1}", "Password", default_configs["Password"]);

                writer.WriteLine();

                writer.WriteLine("# Server MOTD");
                writer.WriteLine("{0}={1}", "MOTD", default_configs["MOTD"]);

                writer.WriteLine();

                writer.WriteLine("# Map file for default gameinstance");
                writer.WriteLine("{0}={1}", "MapPath", default_configs["MapPath"]);

                writer.WriteLine();

                writer.WriteLine("# Rules file for default gameinstance");
                writer.WriteLine("{0}={1}", "RulesFile", default_configs["RulesPath"]);

                writer.WriteLine();

                writer.WriteLine("# Logging the console in Log.log file");
                writer.WriteLine("{0}={1}", "UseLog", default_configs["UseLog"]);

                writer.WriteLine();

                writer.WriteLine("# Nms socket retrieving delay, can have varius effects.");
                writer.WriteLine("# an low setting get the server responsible but need many resources");
                writer.WriteLine("# an high setting get the server not very responsible but do not always need resources");
                writer.WriteLine("# That can affect Timeout but the datas still complete");
                writer.WriteLine("# The recommended value is depending the server needing and hardware, keep it beetween 50~350");
                writer.WriteLine("{0}={1}", "NmsUpdateTime", default_configs["NmsUpdateTime"]);

                writer.WriteLine();

                writer.WriteLine("# Nms message limit, less to 1 makes the Nms unactive/useless");
                writer.WriteLine("{0}={1}", "NmsMaxMessages", default_configs["NmsMaxMessages"]);

                writer.WriteLine();
                writer.WriteLine("{0}={1}", "ShowDebug", default_configs["ShowDebug"]);

                writer.Flush();
            }
        }

        internal static Dictionary<string,string> ReadProperties(string path)
        {
            Dictionary<string,string> dictionary = new Dictionary<string, string>();
            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                foreach (string line in lines)
                {
                    if (line.StartsWith("#") || !line.Contains("="))
                        continue;

                    string[] keyvalue = line.Split(new []{ '=' }, 2);
                    dictionary.Add(keyvalue[0], keyvalue[1]);
                }
            }
            return dictionary;
        }
    }
}
